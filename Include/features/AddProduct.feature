@AddProduct
Feature: Add Product

  Background: User already login
    Given User go to login page
    When User input email field with "testcase@gmail.com"
    And User input password field with "case12345"
    And User tap Masuk button
    Then User successfully login

  Scenario Outline: User want to add product with <condition>
    When User tap Jual button
    And User input product name with <name>
    And User input product price with <price>
    And User choose product category
    And User input product location with <location>
    And User input product description with <desc>
    And User input product image
    And User tap terbitkan button
    Then User <result> add product
    And User see message of <message>

    Examples: 
      | case_id | condition                 | name        | price       | location   | desc        | result      | message 					|
      | A01     | successfully              | 'Iphone 15' | '50000000'  | 'Semarang' | 'Kebal Api' | successfully| successfully 		|
      | A02     | product name empty        | ''          | '50000000'  | 'Semarang' | 'Kebal Api' | failed      | name empty 			|
      | A03     | product price empty       | 'Iphone 15' | ''				  | 'Semarang' | 'Kebal Api' | failed			 | price empty 			|
      | A04     | product location empty    | 'Iphone 15' | '50000000'  | ''         | 'Kebal Api' | failed      | location empty 	|
      | A05     | product description empty | 'Iphone 15' | '50000000'  | 'Semarang' | ''          | failed      | description empty|
