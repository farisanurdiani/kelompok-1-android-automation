@BidProduct
Feature: Bid-Product

  Background: user already login
    Given User go to login page
    When User input email field with "djakafarizki.fl@gmail.com"
    And User input password field with "berapahari05"
    And User tap Masuk button
    Then User successfully login
 
  Scenario Outline: User can bid a product with <condition>
    When User tap <product> product
    And User tap button saya tertarik dan ingin nego
    And User input <bid_price> bid price
    And User tap button kirim
    Then User <result> bid product

    Examples: 
      | case_id  | condition      | product   | bid_price    | result        |
      | A01      | lower price    | macbook 	| '5000'       | successfully  |
      | A02      | higher price	  | macbook   | '12000000'   | successfully  |
      | A03      | 0 price        | macbook 	| '0'          | successfully  |
      | A04      | alphabet price | macbook 	| '-100'  		 | successfully  |
      | A05      | -100 price     | macbook 	| 'asdfgh'     | failed  			 |
