@Editproduct
Feature: edit product

  Background: user already login
    Given User go to login page
    When User input email field with valid email
    And User input password field with valid password
    And User tap Masuk button
    And User successfully login

    
  Scenario Outline: User can edit product with <condition>
   	When User tap daftar jual saya button
   	And User tap product card
   	And User input name product with <name>
   	And User input price product with <price>   
   	And User choose category product
   	And User input location with <location>
   	And User input description with <description>
   	And User input image
   	And User tap button perbarui produk
   	Then User <result> edit product
   	
   	Examples:
   	 | case_id | condition           			| price    	| description		|		name		| location  	| result			|
		 | E01     | successfully  						| '8000'		| 'warna warni'	|		'ayam'	|	'Bandung'		| successfully|
     | E02     | empty product name    	  | '8000'		| 'warna warni'	|		''			|	'Bandung'		| failed			|
     | E03     | empty price							| ''				| 'warna warni'	|		'ayam'	|	'Bandung'		| failed			|
     | E04     | empty description				| '8000'		|	''						|		'ayam'  |	'Bandung' 	| failed 			|
     | E05		 | empty location						| '8000' 	 	| 'warna warni' |   'ayam' 	| '' 					| failed			|
     
     