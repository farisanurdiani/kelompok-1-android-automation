@Profile
Feature: Profile

  Background: User already login
    Given User go to login page
    When User input email field with "gumiwang.nc@gmail.com"
    And User input password field with "987654321"
    And User tap Masuk button
    Then User successfully login

  Scenario Outline: User want to update profile details with <condition>
    When user tap on Pen icon
    And user tap on Name field
    And user delete and enter new data in Name field with "Kelompok 1"
    And user tap button Save Name
    And user tap on Phone field
    And user delete and enter new data in Phone number field with <phone> phone
    And user tap button Save Phone
    And user tap on City field
    And user delete and enter new data in City field  with "Jakarta Raya"
    And user tap button Save City
    And user tap on Address field
    And user delete and enter new data in Address field with <address> address
    And user tap button Save Address
   Then user <result> edit profile

    Examples: 
      | case_id | condition           | phone   | address | result      |
      | P01     | correct credential  | valid   | valid   | succesfully |
      | P02     | address field empty | valid   | empty   | failed      |
      | P03     | invalid number      | invalid | valid   | succesfully |


   Scenario: User wants to add Photo Profile
    When user tap on Pen icon
    And user tap on button Photo
    And user tap on button Gallery
    And user tap on Image/Photo
    Then a photo/image that user choosed will show on profile

	Scenario: User want to edit password
    When user tap on Pen icon
    And user tap on password button
    And user tap on old password field "987654321"
    And user tap on new password field "987654321"
    And user tap on Confirm new password "987654321"
    Then user tap button save password
