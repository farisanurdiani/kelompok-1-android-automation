@Register
Feature: Register

 Scenario Outline: User can register an account with <condition>
    Given User go to register page 
    When User input name field with <name> name in Register Page
    And User input email field with <email> email in Register Page
    And User input password field with <password> password in Register Page
    And User input nomor hp field with <nomor hp> nomor hp in Register Page
    And User input kota field with <kota> kota in Register Page
    And User input alamat field with <alamat> alamat in Register Page
    And User tap Daftar button
    Then User <result> register

    Examples:
    | case_id	|	condition						|	name	|	email		| password|	nomor hp| kota	|	alamat	|	result 			|
    #| B01		 	| correct terms				| valid	| valid		| valid 	| valid 	|	valid |	valid 	| successfully|
    #| B02		 	| name field empty		| empty	| valid		|	valid 	| valid		|	valid	|	valid		| failed			|
    | B03			| registered email		| valid	| invalid	| valid 	| valid		|	valid |	valid 	| failed			|
    #| B04			| email field empty		|	valid	| empty 	| valid 	|	valid 	|	valid |	valid 	| failed			|
    #| B05			| password field empty| valid	| valid 	| empty		|	valid		|	valid |	valid		| failed			|
   #	| B06			| nomor hp field empty| valid	| valid 	| valid		|	empty		|	valid |	valid 	| failed			|
  #	| B07			| kota field empty		| valid	| valid 	| valid		|	valid		|	empty |	valid		| failed			|
  #	| B08			| alamat field empty	| valid	| valid 	| valid		|	valid		|	valid |	empty		| failed			|