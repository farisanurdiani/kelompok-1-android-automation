package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Bidproduct {
	
	@When("User tap macbook product")
	public void user_tap_macbook_product() {
		Mobile.tap(findTestObject('BidProduct/button_Beranda'), 0)
		Mobile.tap(findTestObject('BidProduct/button_Semua'), 0)
		Mobile.tap(findTestObject('BidProduct/button_macbook'), 0)
	}

	@When("User tap button saya tertarik dan ingin nego")
	public void user_tap_button_saya_tertarik_dan_ingin_nego() {
		Mobile.tap(findTestObject('BidProduct/button_Saya Tertarik dan Ingin Nego'), 0)
	}

	@When("User input {string} bid price")
	public void user_input_bid_price(String bid_price) {
		Mobile.setText(findTestObject('BidProduct/input_field_harga_tawar'), bid_price, 0)
	}

	@When("User tap button kirim")
	public void user_tap_button_kirim() {
		Mobile.tap(findTestObject('BidProduct/button_Kirim'), 0)
	}

	@Then("User successfully bid product")
	public void user_successfully_bid_product() {
		Mobile.verifyElementVisible(findTestObject('Product_info_page/text_harga_tawarmu_berhasil_dikirim_ke_penjual'), 0)
	}

	@Then("User failed bid product")
	public void user_failed_bid_product() {
		Mobile.verifyElementVisible(findTestObject('BidProduct/button_Saya Tertarik dan Ingin Nego'), 0)
	}
}

