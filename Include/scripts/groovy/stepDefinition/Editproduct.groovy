package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable

public class EditProduct {
	@When("User tap daftar jual saya button")
	public void user_tap_daftar_jual_saya_button() {
		Mobile.tap(findTestObject('Akun_page/button_daftar_jual_saya'), 30)
	}

	@When("User tap product card")
	public void user_tap_product_card() {
		Mobile.tap(findTestObject('DaftarJual_page/product_List1'), 0)
	}

	@When("User input name product with {string}")
	public void user_input_name_product_with(String name) {
		Mobile.setText(findTestObject('Ubah_produk_page/input_field_nama_produk'), name, 0)
	}

	@When("User input price product with {string}")
	public void user_input_price_product_with(String price) {
		Mobile.setText(findTestObject('Ubah_produk_page/input_field_harga_produk'), price, 0)
	}

	@When("User choose category product")
	public void user_choose_category_product() {
		Mobile.tap(findTestObject('Ubah_produk_page/Ddropdown_category'), 0)
		Mobile.tap(findTestObject('Ubah_produk_page/category_elektronik'), 0)
	}

	@When("User input description with {string}")
	public void user_input_description_with(String description) {
		Mobile.setText(findTestObject('Ubah_produk_page/input_field_desc'), description, 0)
	}

	@When("User input image")
	public void user_input_image() {
		Mobile.tap(findTestObject('Ubah_produk_page/btn_image'), 0)
		Mobile.tap(findTestObject('Ubah_produk_page/btn_galeri'), 0)
		Mobile.tap(findTestObject('Ubah_produk_page/image_produk'), 0)
	}

	@When("User input location with {string}")
	public void user_input_location_with(String location) {
		Mobile.setText(findTestObject('Ubah_produk_page/input_field_lokasi'), location, 0)
	}
	@When("User tap button perbarui produk")
	public void user_tap_button_perbarui_produk() {
		Mobile.tap(findTestObject('Ubah_produk_page/button_perbarui_produk'), 0)
	}

	@Then("User successfully edit product")
	public void user_successfully_edit_product() {
		Mobile.verifyElementVisible(findTestObject('DaftarJual_page/message_EditSuccess'), 0)
	}

	@Then("User failed edit product")
	public void user_failed_edit_product() {
		Mobile.verifyElementVisible(findTestObject('Ubah_produk_page/input_field_nama_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Ubah_produk_page/input_field_harga_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Ubah_produk_page/input_field_desc'), 0)
		Mobile.verifyElementVisible(findTestObject('Ubah_produk_page/input_field_lokasi'), 0)
	}
}


