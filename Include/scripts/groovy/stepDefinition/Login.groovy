package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Login {
	@Given("User go to login page")
	public void user_go_to_login_page() {
		Mobile.startApplication('APK/app-release.apk', true)
		Mobile.tap(findTestObject('Menu_bar/button_Akun'), 0)
		Mobile.tap(findTestObject('Akun_page/button_masuk'), 0)
		Mobile.verifyElementVisible(findTestObject('Login_page/text_masuk'), 0)
		Mobile.verifyElementVisible(findTestObject('Login_page/input_email'), 0)
		Mobile.verifyElementVisible(findTestObject('Login_page/input_password'), 0)
	}

	@When("User tap Masuk button")
	public void user_tap_Masuk_button() {
		Mobile.tap(findTestObject('Login_page/button_masuk'), 0)
	}

	@When("User input email field with (.*) email")
	public void user_input_email_field_with_email(String email){
		if(email=='valid') {
			Mobile.setText(findTestObject('Login_page/input_email'), 'gumiwang.nc@gmail.com', 0)
		}else if(email=='invalid') {
			Mobile.setText(findTestObject('Login_page/input_email'), 'gumiwang.binar@gmail.com', 0)
		}else if(email=='empty') {
			Mobile.setText(findTestObject('Login_page/input_email'), '', 0)
		}
	}

	@When("User input password field with (.*) password")
	public void user_input_password_field_with_password(String password) {
		if(password=='valid') {
			Mobile.setText(findTestObject('Login_page/input_password'), '987654321', 0)
		}else if(password=='invalid') {
			Mobile.setText(findTestObject('Login_page/input_password'), '123456789', 0)
		}else if(password=='empty') {
			Mobile.setText(findTestObject('Login_page/input_password'), '', 0)
		}
	}

	@Then("User (.*) login")
	public void user_login(String result) {
		if(result=='successfully') {
			Mobile.verifyElementVisible(findTestObject('Akun_page/text_akun_saya'), 0)
			Mobile.verifyElementVisible(findTestObject('Akun_page/button_keluar'), 0)
			Mobile.verifyElementVisible(findTestObject('Akun_page/button_daftar_jual_saya'), 0)
			Mobile.verifyElementVisible(findTestObject('Akun_page/button_edit_profile'), 0)
		}else if(result=='failed') {
			Mobile.verifyElementVisible(findTestObject('Login_page/text_masuk'), 0)
			Mobile.verifyElementVisible(findTestObject('Login_page/button_masuk'), 0)
			Mobile.verifyElementVisible(findTestObject('Login_page/input_email'), 0)
			Mobile.verifyElementVisible(findTestObject('Login_page/input_password'), 0)
		}
	}
	@When("User input email field with {string}")
	public void user_input_email_field_with(String email) {
		Mobile.setText(findTestObject('Login_page/input_email'), email, 0)
	}
	@When("User input password field with {string}")
	public void user_input_password_field_with(String password) {
		Mobile.setText(findTestObject('Login_page/input_password'), password, 0)
	}
}
