package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils



class Profile {
	@When("user tap on Pen icon")
	public void user_tap_on_Pen_icon() {
		Mobile.tap(findTestObject('Akun_page/button_edit_profile'), 0)
	}

	@When("user tap on Name field")
	public void user_tap_on_Name_field() {
		Mobile.tap(findTestObject('Profile_page/button_nama'), 0)
	}

	@When("user delete and enter new data in Name field with {string}")
	public void user_delete_and_enter_new_data_in_Name_field_with(String nama) {
		Mobile.setText(findTestObject('Profile_page/edit_nama'),nama, 0)
	}

	@When("user tap button Save Name")
	public void user_tap_button_Save_Name() {
		Mobile.tap(findTestObject('Profile_page/button_save'), 0)
	}

	@When("user tap on Phone field")
	public void user_tap_on_Phone_field() {
		Mobile.tap(findTestObject('Profile_page/button_hp'), 0)
	}

	@When("user delete and enter new data in Phone number field with (.*) phone")
	public void user_delete_and_enter_new_data_in_Phone_number_field_with_phone(String phone) {
		if(phone=='valid') {
			Mobile.setText(findTestObject('Profile_page/edit_phone'), '081111134111', 0)
		}else if(phone=='invalid') {
			Mobile.setText(findTestObject('Profile_page/edit_phone'), '08qwerty789', 0)
		}
	}

	@When("user tap button Save Phone")
	public void user_tap_button_Save_Phone() {
		Mobile.tap(findTestObject('Profile_page/button_save'), 0)
	}

	@When("user tap on City field")
	public void user_tap_on_City_field() {
		Mobile.tap(findTestObject('Profile_page/button_kota'), 0)
	}

	@When("user delete and enter new data in City field  with {string}")
	public void user_delete_and_enter_new_data_in_City_field_with(String city) {
		Mobile.setText(findTestObject('Profile_page/edit_kota'),city, 0)
	}


	@When("user tap button Save City")
	public void user_tap_button_Save_City() {
		Mobile.tap(findTestObject('Profile_page/button_save'), 0)
	}

	@When("user tap on Address field")
	public void user_tap_on_Address_field() {
		Mobile.tap(findTestObject('Profile_page/button_alamat'), 0)
	}

	@When("user delete and enter new data in Address field with (.*) address")
	public void user_delete_and_enter_new_data_in_Address_field_with_address(String address) {
		if(address=='valid') {
			Mobile.setText(findTestObject('Profile_page/edit_alamat'), 'Jln Binar Academy Wave 6', 0)
		}else if(address=='empty') {
			Mobile.setText(findTestObject('Profile_page/edit_alamat'), '', 0)
		}
	}

	@When("user tap button Save Address")
	public void user_tap_button_Save_Address() {
		Mobile.tap(findTestObject('Profile_page/button_save'), 0)
	}

	@Then("user (.*) edit profile")
	public void user_edit_profile(String result) {
		if(result=='succesfully') {
			Mobile.verifyElementVisible(findTestObject('Profile_page/Ver_Info_Profil'), 0)
		}else if(result=='failed') {
			Mobile.verifyElementVisible(findTestObject('Profile_page/verification_wajib_diisi'), 0)
			Mobile.verifyElementVisible(findTestObject('Profile_page/verification_warning'), 0)
		}
	}

	@When("user tap on button Photo")
	public void user_tap_on_button_Photo() {
		Mobile.tap(findTestObject('Profile_page/button_photo'), 0)
	}

	@When("user tap on button Gallery")
	public void user_tap_on_button_Gallery() {
		Mobile.tap(findTestObject('Profile_page/button_gallery'), 0)
	}

	@When("user tap on Image\\/Photo")
	public void user_tap_on_Image_Photo() {
		Mobile.tap(findTestObject('Profile_page/button_image'), 0)
	}

	@Then("a photo\\/image that user choosed will show on profile")
	public void a_photo_image_that_user_choosed_will_show_on_profile() {
		Mobile.verifyElementVisible(findTestObject('Profile_page/Ver_Info_Profil'), 0)
	}


	@When("user tap on password button")
	public void user_tap_on_password_button() {
		Mobile.tap(findTestObject('Profile_page/button_password'), 0)
	}

	@When("user tap on old password field {string}")
	public void user_tap_on_old_password_field(String old) {
		Mobile.setText(findTestObject('Profile_page/edit_old_password'),old, 0)
	}

	@When("user tap on new password field {string}")
	public void user_tap_on_new_password_field(String pass) {
		Mobile.setText(findTestObject('Profile_page/edit_new_password'),pass, 0)
	}

	@When("user tap on Confirm new password {string}")
	public void user_tap_on_Confirm_new_password(String confirm) {
		Mobile.setText(findTestObject('Profile_page/confirm_password'),confirm, 0)
	}

	@Then("user tap button save password")
	public void user_tap_button_save_password() {
		Mobile.tap(findTestObject('Profile_page/button_save_password'), 0)
	}
}


