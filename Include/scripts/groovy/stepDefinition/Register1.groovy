package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.commons.lang3.RandomStringUtils


public class Register {
	@Given("User go to register page")
	public void user_go_to_register_page() {
		Mobile.startApplication('APK/app-release.apk', true)
		Mobile.tap(findTestObject('Menu_bar/button_akun'), 0)
		Mobile.verifyElementExist(findTestObject('Akun_page/text_akun_saya'), 0)
		Mobile.tap(findTestObject('Akun_page/button_masuk'), 0)
		Mobile.verifyElementExist(findTestObject('Login_page/text_masuk'), 0, FailureHandling.STOP_ON_FAILURE)
		Mobile.tap(findTestObject('Login_page/button_daftar'), 0)
	}

	@Then("User successfully register")
	public void user_successfully_register() {
		Mobile.verifyElementExist(findTestObject('Akun_page/button_daftar_jual_saya'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User failed register")
	public void user_failed_register() {
		Mobile.verifyElementExist(findTestObject('Register_page/text_belumpunyaakun'), 0)

	}

	@When("User tap Daftar button")
	public void user_tap_Daftar_button() {
		Mobile.tap(findTestObject('Register_page/button_daftar'), 0)
	}

	@When("User input name field with valid name in Register Page")
	public void user_input_name_field_with_valid_name_in_Register_Page(){
		Mobile.setText(findTestObject('Register_page/input_nama'), RandomStringUtils.randomAlphabetic(5), 0)
	}

	@When("User input name field with empty name in Register Page")
	public void user_input_name_field_with_empty_name_in_Register_Page(){
		Mobile.setText(findTestObject('Register_page/input_nama'), '', 0)
	}

	@When("User input email field with valid email in Register Page")
	public void user_input_email_field_with_valid_email_in_Register_Page(){
		Mobile.setText(findTestObject('Register_page/input_email'), RandomStringUtils.randomAlphanumeric(9) + "@gmail.com", 0)
	}

	@When("User input email field with invalid email in Register Page")
	public void user_input_email_field_with_invalid_email_in_Register_Page(){
		Mobile.setText(findTestObject('Register_page/input_email'), 'farisa1994n@gmail.com', 0)
	}

	@When("User input email field with empty email in Register Page")
	public void user_input_email_field_with_empty_email_in_Register_Page(){
		Mobile.setText(findTestObject('Register_page/input_email'), '', 0)
	}

	@When("User input password field with valid password in Register Page")
	public void user_input_password_field_with_in_valid_password_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_password'), RandomStringUtils.randomAlphanumeric(8), 0)
	}

	@When("User input password field with empty password in Register Page")
	public void user_input_password_field_with_in_empty_password_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_password'), '', 0)
	}

	@When("User input nomor hp field with valid nomor hp in Register Page")
	public void user_input_nomor_hp_field_with_valid_nomor_hp_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_nomor_hp'), RandomStringUtils.randomNumeric(12), 0)
	}

	@When("User input nomor hp field with empty nomor hp in Register Page")
	public void user_input_nomor_hp_field_with_empty_nomor_hp_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_nomor_hp'), '', 0)
	}

	@When("User input kota field with valid kota in Register Page")
	public void user_input_kota_field_with_valid_kota_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_kota'), 'Seoul', 0)
	}

	@When("User input kota field with empty kota in Register Page")
	public void user_input_kota_field_with_empty_kota_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_kota'), '', 0)
	}

	@When("User input alamat field with valid alamat in Register Page")
	public void user_input_alamat_field_with_valid_alamat_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_alamat'), 'Gangnam', 0)
	}

	@When("User input alamat field with empty alamat in Register Page")
	public void user_input_alamat_field_with_empty_alamat_in_Register_Page() {
		Mobile.setText(findTestObject('Register_page/input_alamat'), '', 0)
	}
}