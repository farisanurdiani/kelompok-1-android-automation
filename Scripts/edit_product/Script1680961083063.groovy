import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Akun_page/button_daftar_jual_saya'), 30)

Mobile.tap(findTestObject('DaftarJual_page/product_List1'), 0)

Mobile.setText(findTestObject('Ubah_produk_page/input_field_nama_produk'), '', 0)

Mobile.setText(findTestObject('Ubah_produk_page/input_field_harga_produk'), '', 0)

Mobile.tap(findTestObject('Ubah_produk_page/dropdown_ kategori'), 0)

Mobile.tap(findTestObject('Ubah_produk_page/button_kategori_elektronik'), 0)

Mobile.setText(findTestObject('Ubah_produk_page/input_field_lokasi'), '', 0)

Mobile.setText(findTestObject('Ubah_produk_page/input_field_deskripsi'), '', 0)

Mobile.tap(findTestObject('Ubah_produk_page/button_image'), 0)

Mobile.tap(findTestObject('Ubah_produk_page/button_galeri'), 0)

Mobile.tap(findTestObject('Ubah_produk_page/image'), 0)

Mobile.tap(findTestObject('Ubah_produk_page/button_perbarui_produk'), 0)

